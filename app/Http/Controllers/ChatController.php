<?php

namespace App\Http\Controllers;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use Illuminate\Http\Request;
use App\Events\NewChatMessage;

class ChatController extends Controller
{
    public function rooms(Request $request)
    {
        return ChatRoom::all();
    }

    public function messages(Request $request, $room_id)
    {
        return ChatMessage::where('chat_room_id', $room_id)->with('user')
            ->orderBy('created_at', 'ASC')
            ->get();
    }

    public function newMessage(Request $request, $room_id)
    {
        $request->validate(['message' => 'required']);
        $message = new ChatMessage;
        $message->chat_room_id = $room_id;
        $message->user_id = auth()->id();
        $message->message = $request->message;
        $message->save();

        broadcast(new NewChatMessage( $message ))->toOthers();

        return $message;
    }

}
