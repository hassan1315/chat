<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChatRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Hassan Elhawary',
            'email' => 'hassan@gmail.com',
            'password' => bcrypt(123456),
        ]);

        DB::table('users')->insert([
            'name' => 'Mohamed Ahmed',
            'email' => 'mohamed@gmail.com',
            'password' => bcrypt(123456),
        ]);

        DB::table('chat_rooms')->insert([
            'name' => 'Global',
        ]);

        DB::table('chat_rooms')->insert([
            'name' => 'Technical',
        ]);

        DB::table('chat_rooms')->insert([
            'name' => 'IT',
        ]);

        DB::table('chat_rooms')->insert([
            'name' => 'Accounting',
        ]);

        DB::table('chat_rooms')->insert([
            'name' => 'Mangments',
        ]);

        DB::table('chat_rooms')->insert([
            'name' => 'Social Media',
        ]);
    }
}
